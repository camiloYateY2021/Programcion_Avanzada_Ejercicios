package EjerciciosParcial;

import javax.swing.*;
import java.util.Scanner;

public class CadenaInvertida_9 {

    public static void main(String[] args) {
        /*
         * Realice un Programa Java, que lea un String de max 10 caracteres,
         * introduzca cada carácter en un arreglo, muestre de manera invertida
         * carácter a carácter el arreglo, es decir, la cadena se mostrara de
         * manera invertida.
         * */
//        String cadena = JOptionPane.showInputDialog(null, "Ingresa una cadena de 10 caracteres");

        Scanner cadena1 = new Scanner(System.in);
        System.out.println("Ingresa una palabra de 10 caracteres");
        String cadena = cadena1.nextLine();

        if (cadena.length() <= 10) {
            char[] aCaracteres = cadena.toCharArray();
            for (int x = aCaracteres.length-1; x >= 0; x--)
                System.out.println("[" + x + "] " + aCaracteres[x]);


        } else {
            JOptionPane.showMessageDialog(null, "Por favor Ingresa una cadena de 10 caracteres");

        }
    }

}
