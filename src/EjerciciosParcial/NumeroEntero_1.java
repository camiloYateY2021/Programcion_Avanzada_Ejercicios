package EjerciciosParcial;

import javax.swing.*;
import java.util.Scanner;


//Taller 1_3-27agosto2018
public class NumeroEntero_1 {


    public static void main(String[] args) {

// Realice un Programa Java, que lea un numero entero (de max 5 cifras), muestre cifra a
// cifra el número.
// Restricción: NO haga tratamientos String en el programa ni al número.

        //haciendo casteo a entero
//        int numero = Integer.parseInt(JOptionPane.showInputDialog(null,"Ingresa tu numero de cinco cifras"));

        Scanner input = new Scanner(System.in);

        int numero, d1, d2, d3, d4, d5;

        System.out.print("\nIngrese un número de 5 dígitos: ");
        numero = input.nextInt();

        d1 = numero % 10;
        d2 = numero % 100 / 10;
        d3 = numero % 1000 / 100;
        d4 = numero % 10000 / 1000;
        d5 = numero % 100000 / 10000;

        System.out.print("\nEl número ingresado está compuesto por los siguiente dígitos:\n");
        System.out.printf("Cifra 1 :%d\n   Cifra 2 :%d\n   Cifra 3 :%d\n   Cifra 4 :%d\n   Cifra 5 :%d\n", d5, d4, d3, d2, d1);

    }
}
