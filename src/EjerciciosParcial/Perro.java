package EjerciciosParcial;

public class Perro {


    // atributos

    String nombre = "marcos";
    int patas = 4;
    String pelo = "golden";
    String raza = "lobo";

    public Perro(String nombre, int patas, String pelo, String raza) {
        this.nombre = nombre;
        this.patas = patas;
        this.pelo = pelo;
        this.raza = raza;
    }

    public void datosperro(String nombre, int patas, String pelo, String raza){
        System.out.println("El nombre del perro es :" + nombre);
        System.out.println("Este perro tiene :"+ patas);
        System.out.println("Este perro tiene el pero " + pelo);
        System.out.println("Este perro es de raza " + raza);

    }

    //metodos funciones

      // visibilidad // tipo de retorno // nombre (param)

    public void correr(int velocidad){
        System.out.println("La velocidad del perro es " + velocidad);
    }

    public void comer(String tipocomida){
        System.out.println("Este perro se la pasa comeindo esto" + tipocomida);
    }




}
