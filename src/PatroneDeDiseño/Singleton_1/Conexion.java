package PatroneDeDiseño.Singleton_1;

public class Conexion {

    //Por lo general el Framework Spring utiliza este patron para las dependencias
    //todo ventaja => rendiemiento de memoria
    //todo desventaja => no podemos abusar de este patron

//   todo Documentacion
    //https://sourcemaking.com/design_patterns/singleton
    //https://www.tutorialspoint.com/design_pattern/singleton_pattern.htm

    /// 1 delcaracion
    private static Conexion instancia;
    // private static Conexion instancia 0 new Conexion();

    // para evitar instancias mediante el operador "New"
    private Conexion() {

    }
    //Para obtener la instancia unicamente de este metodo
    // notese la palabra reservada "Static" hace posible  el acceso mediante  Clase.metodo

    public static Conexion getInstancia() {
        if (instancia == null) {
            instancia = new Conexion();
        }
        return instancia;
    }


    //metodo prueba
    public void conectar() {
        System.out.println("Me conecte a la base de datos");
    }

    // metod prueba
    public void desconectar() {

        System.out.println("Me desconecte de la base de datos");
    }
}
