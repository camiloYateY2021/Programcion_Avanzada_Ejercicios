package PatroneDeDiseño.Prototype_4;

public class CuentaAHimplementacion implements ICuenta {
    private String tipo;
    private double monto;

    public CuentaAHimplementacion() {
        tipo = "Ahorro";
    }

    @Override
    public ICuenta clonar() {

        CuentaAHimplementacion cuenta = null;

        try {
            cuenta = (CuentaAHimplementacion) clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();

        }
        return cuenta;
    }


    @Override
    public String toString() {
        return "CuentaAHimplementacion [tipo = +" + tipo + ", monto=" + monto + "]";
    }

    public void setMonto(double monto) {

        this.monto = monto;
    }

    public double getMonto() {
        return monto;
    }
}
